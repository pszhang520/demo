package com.itheima.domain;

public class User {
    private int id;
    private String name;
    private String gender;
    private int age;
    private int did;
    private String content;

    public User() {
    }

    public User(int id, String name, String gender, int age, int did, String content) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.did = did;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", did=" + did +
                ", content='" + content + '\'' +
                '}';
    }
}
