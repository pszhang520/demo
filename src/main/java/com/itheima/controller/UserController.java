package com.itheima.controller;

import com.itheima.domain.KeShi;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/pages/user")
public class UserController {

    @Autowired
    private UserService userService;


    @ResponseBody
    @RequestMapping("/findKind")
    public List<KeShi> findKind(HttpServletRequest request) {
        List<KeShi> list = userService.findKind();
        request.getSession().setAttribute("list", list);
        return list;
    }

}
